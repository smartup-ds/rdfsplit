/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.split.common;

/**
 * Constant Variables initialization.
 * @author mafragias
 */
public class Resources {

    public static final String RDF = "rdf";
    public static final String CSV = "csv";
    public static final String TSV = "tsv";
    public static final String TRIG = "trig";
    public static final String TTL = "ttl";
    public static final String N3 = "n3";
    public static final String XML = "xml";
    public static final String NT = "nt";
    public static final String JSON = "json";
    public static final String SPLIT = "split";
    
}
